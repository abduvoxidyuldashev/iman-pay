import cls from "./OurPartners.module.scss";
import useTranslation from "next-translate/useTranslation";
import Image from "next/image";
import { SamplePrevArrow, SampleNextArrow } from "../Arrows/Arrows";
import Slider from "react-slick";
import { Container } from "@mui/material";
import { categories } from "./data";

export default function OurPartners() {
  const { t } = useTranslation("common");
  const responsive = [
    {
      breakpoint: 768,
      settings: {
        speed: 700,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
      },
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
      },
    },
  ];

  return (
    <div className={cls.root} id="partners">
      <Container className={cls.container}>
        <div className={cls.row}>
          <h2>{t("Наши партнеры")}</h2>
          <div className={cls.slider}>
            <Slider
              {...{
                dots: false,
                infinite: true,
                speed: 500,
                lazyLoad: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                nextArrow: <SampleNextArrow styles={cls.styleNext} />,
                prevArrow: <SamplePrevArrow styles={cls.stylePrev} />,
                responsive,
              }}
            >
              {categories.map(({ name, url }, i) => (
                <div key={"partners" + i} className={cls.item}>
                  <div className={cls.card}>
                    <div className={cls.img}>
                      <Image
                        width={250}
                        height={70}
                        objectFit="contain"
                        src={`/images/business/${url}`}
                        alt={name}
                      />
                    </div>
                  </div>
                </div>
              ))}
            </Slider>
          </div>
        </div>
      </Container>
    </div>
  );
}
