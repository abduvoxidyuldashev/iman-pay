import { Button, Container } from "@mui/material";
import useTranslation from "next-translate/useTranslation";
import cls from "./NewWaySell.module.scss";
import Image from "next/image";
import { useWindowWidth } from "hooks/useWindowWith";
import Link from "next/link";

export default function NewWaySell() {
  const { t } = useTranslation("common");
  const width = useWindowWidth();

  return (
    <div className={cls.root}>
      <Container className={cls.container}>
        <div className={cls.row}>
          <div className={cls.colFirst}>
            <h1>{t("Новый способ продажи")}</h1>
            <p>
              {t(
                "“Iman Merchant” - это инновационное решение для торговых партнёров, позволяющее быстро и легко осуществлять продажи."
              )}
            </p>
            <div className={cls.buttons}>
              <Link href="/">
                <a>
                  <Button>{t("Как это работает")}</Button>
                </a>
              </Link>
              <Link href="/">
                <a>
                  <Button>{t("Стать партнёром")}</Button>
                </a>
              </Link>
            </div>
          </div>
          <div className={cls.colSecond}>
            <div className={cls.img}>
              <Image
                src="/images/business/mobile.png"
                alt="banner"
                width={width > 768 ? 302 : 238}
                height={width > 768 ? 653 : 515}
              />
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
