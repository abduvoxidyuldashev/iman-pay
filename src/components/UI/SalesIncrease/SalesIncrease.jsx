import {  Container } from "@mui/material";
import useTranslation from "next-translate/useTranslation";
import cls from "./SalesIncrease.module.scss";
import Image from "next/image";

export default function SalesIncrease() {
  const { t } = useTranslation("common");

  return (
    <div className={cls.root}>
      <Container className={cls.container}>
        <div className={cls.row}>
          <div className={cls.colFirst}>
            <h1>{t("Увеличение продаж")}</h1>
            <div className={cls.mobileBanner}>
              <div className={cls.img}>
                <Image
                  src="/images/business/saleMobile.png"
                  layout="fill"
                  alt="sale"
                />
              </div>
            </div>
            <p>
              {t(
                'Сервис "Iman Pay" будет увеличивать количество покупателей в Вашем магазине за счет активной рекламы, что положительно повлияет на совместные продажи. '
              )}
            </p>
          </div>
          <div className={cls.colSecond}>
            <div className={cls.img}>
              <Image src="/images/business/sale.png" layout="fill" alt="sale" />
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
