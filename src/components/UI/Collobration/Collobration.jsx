import { Container } from "@mui/material";
import useTranslation from "next-translate/useTranslation";
import cls from "./Collobration.module.scss";
import Image from "next/image";

export default function Collobration() {
  const { t } = useTranslation("common");

  return (
    <div className={cls.root}>
      <Container className={cls.container}>
        <div className={cls.row}>
          <div className={cls.col}>
            <h1>{t("Коллаборация брендов")}</h1>
            <p>
              {t(
                "Посредством активной рекламы в социальных сетях (таргет) мы можем закрепить два бренда, что увеличит приток новых пользователей."
              )}
            </p>
          </div>
          <div className={cls.col}>
            <div className={cls.img}>
              <Image
                src="/images/business/brands.png"
                width={496}
                height={202}
                alt="brands"
              />
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
