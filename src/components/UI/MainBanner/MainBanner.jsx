import { useState } from "react";
import cls from "./MainBanner.module.scss";
import useTranslation from "next-translate/useTranslation";
import { Button, Container, Dialog } from "@mui/material";
import Image from "next/image";
import { PlayMarketIcon, RegistrIcon, BuyIcon, UzbIcon } from "../Icons";
import { CloseIcon } from "../Icons";
import InputMask from "../InputMask";
import { useForm } from "react-hook-form";
import { Fade } from "react-reveal";

export default function MainBanner() {
  const { t } = useTranslation("common");
  const [open, setOpen] = useState(false);
  const {
    control,
    formState: { errors },
  } = useForm({
    defaultValues: {
      phone: "",
    },
  });

  return (
    <>
      <Fade>
        <div className={cls.root}>
          <Container className={cls.container}>
            <div className={cls.title}>
              <h2>{t("Новый способ оплаты")}</h2>
            </div>
            <div className={cls.banner}>
              <div className={cls.banner_imgs}>
                <div className={cls.first_img}>
                  <Image
                    src="/images/home/mobile1.png"
                    width={280}
                    height={500}
                    alt="mobile"
                  />
                </div>

                <div className={cls.second_img}>
                  <Image
                    src="/images/home/mobile2.png"
                    width={280}
                    height={550}
                    alt="mobile"
                  />
                </div>
              </div>
              <div className={cls.banner_steps}>
                <div onClick={() => setOpen(true)} className={cls.step}>
                  <div className={cls.step_img}>
                    <PlayMarketIcon />
                  </div>
                  <div>
                    <p className={cls.step_title}>{t("Скачать приложение")}</p>

                    <p className={cls.step_text}>
                      {t("Приложение доступно в App Store и Google Play. ")}
                    </p>
                  </div>
                </div>
                <div className={cls.step}>
                  <div className={cls.step_img}>
                    <RegistrIcon />
                  </div>
                  <div>
                    <p className={cls.step_title}>{t("Пройти регистрацию")}</p>
                    <p className={cls.step_text}>
                      {t(
                        "Для регистрации вам понадобится паспорт / ID-карта и зарплатная карта. "
                      )}
                    </p>
                  </div>
                </div>
                <div className={cls.step}>
                  <div className={cls.step_img}>
                    <BuyIcon />
                  </div>
                  <div>
                    <p className={cls.step_title}>{t("Совершить покупку")}</p>
                    <p className={cls.step_text}>
                      {t(
                        "С полученным лимитом вы можете отправиться в магазины наших партнеров за покупками. "
                      )}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </Container>
        </div>
      </Fade>
      <Dialog
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <div className={cls.dialog}>
          <span onClick={() => setOpen(false)} className={cls.close}>
            <CloseIcon />
          </span>
          <div className={cls.body}>
            <p>
              {t(
                "Укажите свой номер телефона для получение ссылки на скачивание приложения"
              )}
            </p>
            <div className={cls.input}>
              <div className={cls.select}>
                <UzbIcon />
                <p>+998</p>
              </div>
              <InputMask
                mask={`99 999 99 99`}
                placeholder={t("99 999 99 99")}
                control={control}
                name="phone"
                errors={errors}
                required
              />
            </div>
            <Button className={cls.button}>{t("Получить ссылку")}</Button>
          </div>
        </div>
      </Dialog>
    </>
  );
}
