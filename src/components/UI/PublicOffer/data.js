export const responsive = [
  {
    breakpoint: 768,
    settings: {
      speed: 700,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
    },
  },
  {
    breakpoint: 576,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: false,
    },
  },
];
