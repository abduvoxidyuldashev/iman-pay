import cls from "./PublicOffer.module.scss";
import useTranslation from "next-translate/useTranslation";
import { SamplePrevArrow, SampleNextArrow } from "../Arrows/Arrows";
import Slider from "react-slick";
import { Container } from "@mui/material";
import { RightArrowIcon } from "../Icons";
import { responsive } from "./data";
import Link from "next/link";

export default function PublicOffer() {
  const { t } = useTranslation("common");
  const titles = [
    {
      title: "Продажа товаров",
      url: "",
    },
    {
      title: "Финансовая аренда",
      url: "",
    },
    {
      title: "Оказание услуги",
      url: "",
    },
  ];

  return (
    <div className={cls.root} id="public-offer">
      <Container className={cls.container}>
        <div className={cls.row}>
          <div className={cls.breadCrumb}>
            <Link href="/">
              <a>
                <p>{t("Iman Pay ")}</p>
              </a>
            </Link>
            <RightArrowIcon />
            <p>{t("more")}</p>
            <RightArrowIcon />
            <p>{t("Публичная оферта")}</p>
          </div>
          <h2>{t("Публичная оферта")}</h2>
          <p className={cls.description}>
            {t(
              "Вы можете ознакомиться ниже с условиями наших публичных оферт для приобретения товаров или услуг в рассрочку.  "
            )}
          </p>
          <div className={cls.slider}>
            <Slider
              {...{
                dots: false,
                arrows: false,
                infinite: true,
                speed: 500,
                lazyLoad: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                nextArrow: <SampleNextArrow styles={cls.styleNext} />,
                prevArrow: <SamplePrevArrow styles={cls.stylePrev} />,
                responsive,
              }}
            >
              {titles.map((item, i) => (
                <div key={"offer" + i} className={cls.item}>
                  <div className={cls.card}>
                    <h3>{item.title}</h3>
                    <div className={cls.redirect}>
                      <a href="#">
                        <span>{t("Ознакомиться")}</span>
                      </a>
                      <RightArrowIcon />
                    </div>
                  </div>
                </div>
              ))}
            </Slider>
          </div>
        </div>
      </Container>
    </div>
  );
}
