import { Container } from "@mui/material";
import useTranslation from "next-translate/useTranslation";
import cls from "./JointPromotion.module.scss";
import Image from "next/image";

export default function JointPromotion() {
  const { t } = useTranslation("common");

  return (
    <div className={cls.root}>
      <Container className={cls.container}>
        <div className={cls.row}>
          <div className={cls.colFirst}>
            <div className={cls.img}>
              <Image
                src="/images/business/promotion.png"
                layout="fill"
                alt="sale"
              />
            </div>
          </div>
          <div className={cls.colSecond}>
            <h1>{t("Совместное продвижение")}</h1>

            <p>
              {t(
                "Мы готовы инвестировать в совместный маркетинг, продвигая общую идею о покупке через “Iman Pay” в магазинах партнёров.  "
              )}
            </p>
            <div className={cls.mobileBanner}>
              <div className={cls.img}>
                <Image
                  src="/images/business/saleMobile.png"
                  layout="fill"
                  alt="sale"
                />
              </div>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
