import React, { useEffect, useState } from "react";
import { Drawer } from "@mui/material";
import cls from "./RightMenu.module.scss";
import { ImanLogo } from "../Icons";
import CloseIcon from "@mui/icons-material/Close";
import useTranslation from "next-translate/useTranslation";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  useStyles,
} from "./muStyles";
import Link from "next/link";
import { useRouter } from "next/router";

const moreTitles = [
  { title: "about_us", url: "/" },
  { title: "sharia_compliance", url: "/" },
  { title: "public_offer", url: "/" },
  { title: "payment_types", url: "/" },
  { title: "become_investor", url: "/" },
  { title: "contacts", url: "/" },
];

export default function RightMenu({ isOpen, setIsOpen = () => {} }) {
  const classes = useStyles();
  const router = useRouter();
  const [expanded, setExpanded] = useState("");
  const { t } = useTranslation("common");

  useEffect(() => {
    const handleRouteChange = () => {
      setIsOpen(false);
      setExpanded(false);
    };
    router.events.on("routeChangeStart", handleRouteChange);
    return () => {
      router.events.off("routeChangeStart", handleRouteChange);
    };
  }, []);

  const handleChange = (panel) => (_, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const toggleDrawer = () => (event) => {
    if (
      event?.type === "keydown" &&
      (event?.key === "Tab" || event?.key === "Shift")
    ) {
      return;
    }
    setIsOpen(false);
    setExpanded(false);
  };

  return (
    <Drawer
      className={classes.root}
      anchor="right"
      open={isOpen}
      onClose={toggleDrawer()}
    >
      <div className={classes.list} role="presentation">
        <div className={cls.menu}>
          <div className={cls.topData}>
            <Link href={"/"}>
              <a>
                {" "}
                <ImanLogo />
              </a>
            </Link>
            <CloseIcon className={cls.clearIcon} onClick={toggleDrawer()} />
          </div>
          <div className={cls.item}>
            <Accordion
              square
              expanded={expanded === "panel1"}
              onChange={handleChange("panel1")}
            >
              <AccordionSummary
                aria-controls="panel2d-content"
                id="panel2d-header"
                className={cls.acardion}
              >
                <span className={cls.title}>{t("partners")}</span>
                <ExpandMoreIcon />
              </AccordionSummary>
              <AccordionDetails>
                <Link locale={router.locale} href={"/"}>
                  <a>{t("map_magazines")}</a>
                </Link>
              </AccordionDetails>
            </Accordion>
          </div>

          <div className={cls.item}>
            <span className={cls.title}>
              {" "}
              <Link locale={router.locale} href="/">
                <a className={cls.listItem}>{t("for_business")}</a>
              </Link>
            </span>
          </div>

          <div className={cls.item}>
            <Accordion
              square
              expanded={expanded === "panel2"}
              onChange={handleChange("panel2")}
            >
              <AccordionSummary
                aria-controls="panel2d-content"
                id="panel2d-header"
                className={cls.acardion}
              >
                <span className={cls.title}>
                  <a className={cls.listItem}>{t("Еще")}</a>
                </span>
                <ExpandMoreIcon />
              </AccordionSummary>
              {moreTitles.map((item, i) => (
                <AccordionDetails key={i}>
                  <Link locale={router.locale} href={item.url}>
                    <a onClick={toggleDrawer()}>{t(item.title)}</a>
                  </Link>
                </AccordionDetails>
              ))}
            </Accordion>
          </div>

          <div className={cls.item}>
            <Accordion
              square
              expanded={expanded === "panel3"}
              onChange={handleChange("panel3")}
            >
              <AccordionSummary
                aria-controls="panel2d-content"
                id="panel2d-header"
                className={cls.acardion}
              >
                <span className={cls.title}>{t("langugae")}</span>
                <ExpandMoreIcon />
              </AccordionSummary>
              <AccordionDetails>
                <Link scroll={false} href={router.pathname} locale={"en"}>
                  <a>English</a>
                </Link>
              </AccordionDetails>
              <AccordionDetails>
                <Link scroll={false} href={router.pathname} locale={"ru"}>
                  <a>Русский</a>
                </Link>
              </AccordionDetails>
              <AccordionDetails>
                <Link scroll={false} href={router.pathname} locale={"uz"}>
                  <a>O`zbek</a>
                </Link>
              </AccordionDetails>
            </Accordion>
          </div>
        </div>
      </div>
    </Drawer>
  );
}
