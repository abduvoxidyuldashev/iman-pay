import cls from "./CategoriesSlider.module.scss";
import useTranslation from "next-translate/useTranslation";
import Image from "next/image";
import { SamplePrevArrow, SampleNextArrow } from "../Arrows/Arrows";
import Slider from "react-slick";
import { Container } from "@mui/material";
import { categories } from "./data";
import { RightArrowIcon } from "../Icons";

export default function CategoriesSlider() {
  const { t } = useTranslation("common");
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    lazyLoad: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow styles={cls.styleNext} />,
    prevArrow: <SamplePrevArrow styles={cls.stylePrev} />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          speed: 700,
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true,
          dots: true,
          arrows: false,
        },
      },
    ],
  };

  return (
    <div className={cls.root} id="categoriesSlider">
      <Container className={cls.container}>
        <div className={cls.row}>
          <h2>{t("categories")}</h2>
          <div className={cls.slider}>
            <Slider {...settings}>
              {categories.map(({ name, url }, i) => (
                <div key={"category" + i} className={cls.item}>
                  <div className={cls.card}>
                    <div className={cls.body}>
                      <p>{t(name)}</p>
                    </div>
                    <div className={cls.img}>
                      <Image
                        src={`/images/home/${url}`}
                        objectFit="cover"
                        layout="fill"
                      />
                    </div>
                  </div>
                </div>
              ))}
            </Slider>
          </div>
        </div>
      </Container>
      <div className={cls.greenRow}>
        <div className={cls.content}>
          <p>{t("Каталог магазинов")}</p>
          <RightArrowIcon />
        </div>
      </div>
    </div>
  );
}
