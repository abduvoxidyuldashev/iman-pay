import { Button, Container } from "@mui/material";
import useTranslation from "next-translate/useTranslation";
import cls from "./BecomePartner.module.scss";
import Image from "next/image";
import { useWindowWidth } from "hooks/useWindowWith";
import { useForm } from "react-hook-form";
import Input from "../Input";
import { RightArrowIcon } from "../Icons";

import Link from "next/link";
import { useState } from "react";
import Select from "../Select";

export default function BecomePartner() {
  const { t } = useTranslation("common");
  const width = useWindowWidth();
  const {
    handleSubmit,
    register,
    watch,
    control,
    formState: { errors },
    setError,
  } = useForm();

  const onSubmit = (data) => {
    console.log("data ==> ", data);
  };

  const options = [
    {
      name: "Carbon Black",
    },
    {
      name: "SPL Green",
    },

    {
      name: "Ghost White",
    },
  ];

  return (
    <div className={cls.root}>
      <Container className={cls.container}>
        <div className={cls.row}>
          <div className={cls.colFirst}>
            <div className={cls.img}>
              <Image
                src="/images/business/symbol.png"
                width={396}
                height={396}
                alt="symbol"
              />
            </div>
          </div>
          <div className={cls.colSecond}>
            <h2>{t("Станьте партнёром")}</h2>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className={cls.firstRow}>
                <div>
                  <div>
                    <label htmlFor="company">
                      {t("Наименование компании*")}
                    </label>
                    <Input
                      className={cls.input}
                      id="company"
                      placeholder={t("")}
                      name="company"
                      register={register}
                      errors={errors}
                      required
                    />
                  </div>
                  <div className={cls.wrapper}>
                    <label htmlFor="sphere">{t("Сфера деятельности*")}</label>
                    <Input
                      className={cls.input}
                      id="sphere"
                      placeholder={t("")}
                      name="sphere"
                      register={register}
                      errors={errors}
                      required
                    />
                  </div>
                </div>
                <div>
                  <div className={cls.fullname}>
                    <label htmlFor="fullname">{t("ФИО*")}</label>
                    <Input
                      className={cls.input}
                      id="fullname"
                      placeholder={t("")}
                      name="fullname"
                      register={register}
                      errors={errors}
                      required
                    />
                  </div>
                  <div className={cls.wrapper}>
                    <label htmlFor="contact">{t("Контакт для связи*")}</label>
                    <Input
                      className={cls.input}
                      id="contact"
                      placeholder={t("")}
                      name="contact"
                      register={register}
                      errors={errors}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className={cls.secondRow}>
                <div className={cls.wrapper}>
                  <label htmlFor="select">{t("Средний чек*")}</label>
                  <Select
                    options={options}
                    className={cls.input}
                    id="select"
                    placeholder={t("")}
                    name="select"
                    register={register}
                    errors={errors}
                    required
                  />
                </div>
                <div className={cls.wrapper}>
                  <label htmlFor="select2">
                    {t("Среднее количество покупателей/мес*")}
                  </label>
                  <Select
                    options={options}
                    className={cls.input}
                    id="select2"
                    placeholder={t("")}
                    name="select2"
                    register={register}
                    errors={errors}
                    required
                  />
                </div>
              </div>
              <div className={cls.button}>
                <Button type="submit">{t("Присоединиться")}</Button>
              </div>
            </form>
          </div>
          <img
            className={cls.bgImg}
            src="/images/business/symbol.png"
            alt="symbol"
          />
        </div>
      </Container>
      <div className={cls.greenRow}>
        <div className={cls.content}>
          <p>{t("Шаблон договора купли - продажи")}</p>
          <RightArrowIcon />
        </div>
      </div>
    </div>
  );
}
